import { z } from "zod";



export const RegisteSchema = z.object({
    taiKhoan: z.string().nonempty('Vui lòng nhập tài khoản').min(5, 'Tối thiểu 5 ký tự'),
    matKhau: z.string().nonempty('Vui lòng nhập mật khẩu'),
    email: z.string().nonempty('Vui lòng nhập email').email(),
    soDt: z.string().nonempty('Vui lòng nhập số điện thoại'),
    maNhom: z.string().nonempty('Vui lòng nhập mã nhóm'),
    hoTen: z.string().nonempty('Vui lòng nhập họ và tên')
})


export type RegisteSchemaTypes = z.infer<typeof RegisteSchema>