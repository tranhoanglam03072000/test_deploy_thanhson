import { z } from "zod";


export const AccountSchema = z.object({
    taiKhoan: z.string().nonempty('Vui lòng nhập tài khoản').min(5, 'Tối thiểu 5 ký tự'),
    email: z.string().nonempty('Vui lòng nhập email').email(),
    soDt: z.string().nonempty('Vui lòng nhập số điện thoại'),
    maNhom: z.string().nonempty('Vui lòng nhập mã nhóm'),
    hoTen: z.string().nonempty('Vui lòng nhập họ và tên'),
    maLoaiNguoiDung: z.string().nonempty('Vui lòng nhập loại người dùng')
})


export type AccountShemaTypes = z.infer<typeof AccountSchema>