export const PATH = {
    login: '/login',
    register: '/register',
    detail: '/detail/:id',
    booking: '/datVe/:id',
    account: '/account',
    news: '/tintuc',
    promotion: '/khuyenMai',
    admin: '/admin',
    user: '/admin/user',
    film: '/admin/film',
    addfilm:'/admin/film/addfilm',
    editfilm: '/admin/film/edit/:id',
}