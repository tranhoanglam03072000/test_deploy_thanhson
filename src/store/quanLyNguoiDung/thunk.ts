import {createAsyncThunk} from '@reduxjs/toolkit'
import { loginSchemaTypes } from 'schema'

import { quanLyNguoiDungServices } from 'services/quanLyNguoiDungServices'



export const loginThunk = createAsyncThunk('quanLyNguoiDung/loginThunk', async (payload: loginSchemaTypes, {rejectWithValue}) => {
    try {
        const data = await quanLyNguoiDungServices.login(payload)
        return data.data.content
    } catch (err) {
        return rejectWithValue(err)
    }
    
})


export const getUserThunk = createAsyncThunk(
    'quanLyNguoiDung/getUser',
    async(_, {rejectWithValue}) => {
        try {
            const accessToken = localStorage.getItem('accessToken')
            if(accessToken) {
                const data = await quanLyNguoiDungServices.getUser()
                return data.data.content
            }
            return undefined
        } catch (err) {
           return rejectWithValue(err)
        }
    }
)

export const updateUserThunk = createAsyncThunk('quanLyNguoiDung/updateUser', async (payload: any, {rejectWithValue, dispatch}) => {
    try {
       await quanLyNguoiDungServices.updateUser(payload)
        await new Promise((resolve) => {
            setTimeout(resolve, 2000)
        })
       dispatch(getUserThunk())
    } catch (error) {
        return rejectWithValue(error)
    }
})