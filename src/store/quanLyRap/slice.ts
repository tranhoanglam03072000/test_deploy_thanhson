import { createSlice } from '@reduxjs/toolkit'
import { getCinemaSystemThunk, getShowtimeThunk } from './thunk';
import { getCinemaSystem, getShowtime } from 'types';


type QuanLyRapInitialState = {
    heThongRap?: getCinemaSystem[]
    movieDetail?: getShowtime
}
const initialState:QuanLyRapInitialState = {
    heThongRap:[],
    movieDetail:{}
}

const quanLyRapSlice = createSlice({
  name: "quanLyRapSlice",
  initialState,
  reducers: {},
  extraReducers:(builder) => {
      builder
      .addCase(getCinemaSystemThunk.fulfilled, (state, {payload})=> {
        state.heThongRap = payload
      })
      .addCase(getShowtimeThunk.fulfilled, (state, {payload}) => {
        state.movieDetail = payload
      })
  },
});

export const {actions: quanLyRapActions, reducer: quanLyRapReducer} = quanLyRapSlice

