import { createSlice } from '@reduxjs/toolkit'
import { getBookingThunk, thongTinDatVeThunk } from './thunk';
import { bookingTypes, thongTinDatVe} from 'types';


type getBookingInitState = {
    booking?: bookingTypes
    danhSachGheDangDat?: any
    thongTinDatVe?: thongTinDatVe
}
const initialState: getBookingInitState = {
    booking: {},
    danhSachGheDangDat: [],
    thongTinDatVe:{}
  }


const quanLyDatVeSlice = createSlice({
  name: "quanLyDatVeSlice",
  initialState,
  reducers: {
    DAT_VE: (state, action) => {
      // console.log('action: ', action);
      //cập nhật danh sách ghế đang đặt
      // let danhSachGheCapNhat = [...state.danhSachGheDangDat]
      //Xóa ghế khi phát hiện trùng
      // let index = danhSachGheCapNhat.findIndex(gheDD => gheDD.maGhe === payload.maGhe)

      // if(index != -1) {
      //   danhSachGheCapNhat.splice(index, 1)
      // } else {
      //   danhSachGheCapNhat.push(payload.ghe)
      // }
      const index = state.danhSachGheDangDat.findIndex((e: any) => e.maGhe === action.payload.maGhe)
      if(index != -1) {
        state.danhSachGheDangDat.splice(index, 1)
      } else {
        state.danhSachGheDangDat.push(action.payload)
      }

    }
  },
  extraReducers(builder) {
    builder.addCase(getBookingThunk.fulfilled, (state, {payload})=> {
        state.booking = payload
    })
    .addCase(thongTinDatVeThunk.fulfilled, (state, {payload}) => {
      state.thongTinDatVe = payload
    })
  },
});

export const {actions: quanLyDatVeActions, reducer: quanLyDatVeReducer} = quanLyDatVeSlice

