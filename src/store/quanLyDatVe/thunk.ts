import { createAsyncThunk } from "@reduxjs/toolkit";
import { quanLyDatVeServices } from "services/quanLyDatVe";
import { bookingTypes } from "types";



export const getBookingThunk = createAsyncThunk('quanLyDatVe/getBookingThunk', async (id:bookingTypes , {rejectWithValue}) => {
    try {
        const data = await quanLyDatVeServices.getBooking(id)
        return data.data.content
    } catch (err) {
        return rejectWithValue(err)
    }

})


export const thongTinDatVeThunk = createAsyncThunk('quanLyDatVe/thongTinDatVeThunk', async (thongTinDatVe, {rejectWithValue})=> {
    try {
        const accessToken = localStorage.getItem('accessToken')
        if(accessToken) {
            const data = await quanLyDatVeServices.datVe(thongTinDatVe)
            return data.data.content
        }
        return undefined
    } catch (err) {
        return rejectWithValue(err)
    }
})