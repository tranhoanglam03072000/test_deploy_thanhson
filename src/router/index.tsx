
import {RouteObject} from 'react-router-dom'
import { PATH } from 'constant/configPath'
import { Account, Admin, DetailPage, Home, Login, News, Register } from 'pages'
import { AutLayout, MainLayout } from 'components/layouts'
import TicketBookingPage from 'pages/TicketBookingPage'
import Promotion from 'pages/Promotion'
import { AddFilmTemplate, EditFilmTemplate, FilmTemplate, UserTemplate } from 'components/template'

export const router:RouteObject[] = [
    {
        element: <AutLayout/>,
        children: [
            {
                path: PATH.login,
                element: <Login/>
            },
            {
                path: PATH.register,
                element: <Register/>
            },
            
        ]
    },
    {   
        path: '/',
        element: <MainLayout/>,
        children: [
            {
                index: true,
                element: <Home/>,
                
            },
            {
                path: PATH.account,
                element: <Account/>
            },
            {
                path: PATH.booking,
                element: <TicketBookingPage/>
            },
            {
                path: PATH.news,
                element: <News/>
            },
            {
                path: PATH.promotion,
                element: <Promotion/>
            }

        ]
    },
    {
        path: PATH.detail,
        element:<DetailPage/>    
    },
    {
        path: PATH.admin,
        element: <Admin/>,
        children: [
            {
                
                path: PATH.user,
                element: <UserTemplate/>
            },
            {
                path: PATH.film,
                children: [
                    {
                        index: true,
                        element: <FilmTemplate/>,
                    },

                    {
                        path: PATH.addfilm,
                        element: <AddFilmTemplate/>
                    },
                    {
                        path: PATH.editfilm,
                        element: <EditFilmTemplate/>
                    }
                ]
            }
        ]
    }

]