

export type getCinemaSystem = {
    lstCumRap?: any[]
    maHeThongRap?: string
    tenHeThongRap?: string
    logo?: string
    maNhom?: string
  }
  


  export type getShowtime = {
    heThongRapChieu?: any[]
    maPhim?: number
    tenPhim?: string
    biDanh?: string
    trailer?: string
    hinhAnh?: string
    moTa?: string
    maNhom?: string
    hot?: boolean
    dangChieu?: boolean
    sapChieu?: boolean
    ngayKhoiChieu?: string
    danhGia?: number
  }
  