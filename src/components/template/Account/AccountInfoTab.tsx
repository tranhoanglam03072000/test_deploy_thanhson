import { zodResolver } from '@hookform/resolvers/zod'
import { Button } from 'antd'
import { useAuth } from 'hooks'
import { useEffect } from 'react'
import { useForm, SubmitHandler } from 'react-hook-form'
import { AccountSchema, AccountShemaTypes } from 'schema'
import { RootState, useAppDispatch } from 'store'
import { updateUserThunk } from 'store/quanLyNguoiDung/thunk'
import {toast} from 'react-toastify'
import { useSelector } from 'react-redux'



const AccountInfoTab = () => {
    const dispatch = useAppDispatch()
    const { user } = useAuth()
   
    const {isUpdatingUser} = useSelector((state:RootState)=> state.quanLyNguoiDungReducer)

    const { reset, register, handleSubmit, formState: {errors} } = useForm<AccountShemaTypes>({
        mode: 'onChange',
        resolver: zodResolver(AccountSchema)
    })

    useEffect(() => {
        reset({
            ...user,
            soDt: user?.soDT
        })
    }, [user, reset])

    const onSubmit: SubmitHandler<AccountShemaTypes> = (value) => {
        dispatch(updateUserThunk(value)).unwrap().then(()=> {
            toast.success('Lưu thành công')
        })
    }
    return (
        <form className="px-40 flex-col" onSubmit={handleSubmit(onSubmit)}>
            <label className='ml-2' htmlFor="">Tài khoản</label>
            <input className='!w-full text-white !rounded-lg !p-2 !m-2 !bg-slate-600 !border-slate-600' type="text" placeholder='Tài khoản' {...register("taiKhoan")} name='taiKhoan' />
            <p className='text-red-500 ml-2'>{errors?.taiKhoan?.message}</p>

            <label className='ml-2' htmlFor="">Họ và tên</label>
            <input className='!w-full text-white !rounded-lg !p-2 !m-2 !bg-slate-600 !border-slate-600' type="text" placeholder='Họ và tên' {...register("hoTen")} name='hoTen' />
            <p className='text-red-500 ml-2'>{errors?.hoTen?.message}</p>

            <label className='ml-2' htmlFor="">Số điện thoại</label>
            <input className='!w-full text-white !rounded-lg !p-2 !m-2 !bg-slate-600 !border-slate-600' type="text" placeholder='Số điện thoại' {...register("soDt")} name='soDt'/>
            <p className='text-red-500 ml-2'>{errors?.soDt?.message}</p>

            <label className='ml-2' htmlFor="">Email</label>
            <input className='!w-full text-white !rounded-lg !p-2 !m-2 !bg-slate-600 !border-slate-600' type="text" placeholder='Email' {...register("email")} name='email'/>
            <p className='text-red-500 ml-2'>{errors?.email?.message}</p>

            <label className='ml-2' htmlFor="">Mã nhóm</label>
            <input className='!w-full text-white !rounded-lg !p-2 !m-2 !bg-slate-600 !border-slate-600' type="text" placeholder='Mã nhóm' {...register("maNhom")} name='maNhom'/>
            <p className='text-red-500 ml-2'>{errors?.maNhom?.message}</p>

            <label className='ml-2' htmlFor="">Mã loại người dùng</label>
            <input disabled className='!w-full text-white !rounded-lg !p-2 !m-2 !bg-slate-600 !border-slate-600' type="text" placeholder='maLoaiNguoiDung' {...register("maLoaiNguoiDung")} name='maLoaiNguoiDung'/>
            <p className='text-red-500 ml-2'>{errors?.maLoaiNguoiDung?.message}</p>




            <div className='text-right'>
                <Button loading={isUpdatingUser} htmlType='submit' className='mt-[60px] w-[200px] h-[50px]' type='primary'>Lưu thay đổi</Button>
            </div>
   

        </form>
    )
}

export default AccountInfoTab

