import { Tabs } from "antd";
import { Footer, Header } from "components/ui";
import moment from "moment";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { NavLink, generatePath, useParams } from "react-router-dom";
import { RootState, useAppDispatch } from "store";
import { getShowtimeThunk } from "store/quanLyRap/thunk";
import { Rate } from "antd";
import { PATH } from "constant";

const DetailPageTemplate = () => {
  const dispatch = useAppDispatch();
  const { movieDetail } = useSelector(
    (state: RootState) => state.quanLyRapReducer
  );
  console.log("movieDetail?.heThongRapChieu: ", movieDetail?.heThongRapChieu);

  const params = useParams();
  // const {id} = params

  useEffect(() => {
    let { id } = params;
    console.log("id: ", id);
    dispatch(getShowtimeThunk(id));
  }, [dispatch]);

  const renderMovieDetail = () => {
    movieDetail?.moTa;
  };
  renderMovieDetail();


  return (
    <div>
      <Header />

      <div
        style={{
          backgroundImage: `url(${movieDetail?.hinhAnh})`,
          backgroundRepeat: "no-repeat",
          backgroundSize: "100%",
          backgroundPosition: "center",
          // width: "100%",
        }}
        className=""
      >
        <div className="h-full w-full bg-gray-600 bg-clip-padding backdrop-filter backdrop-blur-[3px] bg-opacity-50 pt-40 ">
          <div className="grid grid-cols-12 ">
            <div className="col-span-2 col-start-3 ">
              <img
                className="w-full"
                src={movieDetail?.hinhAnh}
                alt={movieDetail?.tenPhim}
              />
            </div>
            <div className="col-span-3 pl-10 mt-20">
              <p className="text-white">
                Ngày khởi chiếu:{" "}
                {moment(movieDetail?.ngayKhoiChieu).format("DD.MM.YYYY")}
              </p>
              <p className="font-bold text-white text-2xl py-5 leading-4">
                {movieDetail?.tenPhim}
              </p>
              {/* <p className="text-white">{movieDetail?.moTa}</p> */}
            </div>
            <div className="col-start-8 col-span-2">
              <h1 className="text-[#5fd400] ml-[15%]">Đánh giá</h1>
              <h1 className="my-2 ml-[4%] text-[#5fd400]">
                <Rate
                  allowHalf
                  defaultValue={movieDetail?.danhGia}
                  style={{ color: "#5fd400" }}
                />
              </h1>
              <div className={`c100 p${movieDetail?.danhGia * 10} big`}>
                <span>{movieDetail?.danhGia * 10}%</span>

                <div className="slice">
                  <div className="bar" />
                  <div className="fill" />
                </div>
              </div>
            </div>
          </div>

          <div className="pt-20 mx-auto max-w-screen-xl  ">
            <Tabs centered className="bg-[#fff]">
              <Tabs.TabPane tab="Lịch chiếu" key={1}>
                <Tabs tabPosition="left">
                  {movieDetail?.heThongRapChieu?.map((item, index) => {
                    return (
                      <Tabs.TabPane
                        tab={
                          <div className="flex items-center ml-2">
                            <img
                              width={40}
                              height={40}
                              src={item.logo}
                              alt={item.tenHeThongRap}
                            />
                            <div className="ml-2 ">{item.tenHeThongRap}</div>
                          </div>
                        }
                        key={index}
                      >
                        {item?.cumRapChieu?.map((cumRap: any) => {
                          
                          
                          return (
                            <div>
                              <div className="flex items-center">
                                <img
                                  width={40}
                                  height={40}
                                  src={cumRap.hinhAnh}
                                  alt={cumRap.tenCumRap}
                                />
                                <div className="ml-3">
                                  <div className="font-bold text-lg">
                                    {cumRap.tenCumRap}
                                  </div>
                                  <div className="text-[#a6a5a5]">
                                    {cumRap.diaChi}
                                  </div>
                                </div>
                              </div>
                              <div className="grid grid-cols-4">
                                {cumRap?.lichChieuPhim
                                  ?.slice(0, 8)
                                  .map((lichChieu: any) => {
                                    const path = generatePath(PATH.booking, {id: lichChieu.maLichChieu})
                                    return (
                                      <NavLink to={path}
                                        className="col-span-1 text-green-600"
                                        key={lichChieu.maRap}
                                      >
                                        {moment(
                                          lichChieu.ngayChieuGioChieu
                                        ).format("hh.mm - DD.MM.YYYY")}
                                      </NavLink>
                                    );
                                  })}
                              </div>
                            </div>
                          );
                        })}
                      </Tabs.TabPane>
                    );
                  })}
                </Tabs>
              </Tabs.TabPane>
              <Tabs.TabPane tab="Mô tả" key={2}>
                <div className="mx-auto px-80 py-10">{movieDetail?.moTa}</div>
              </Tabs.TabPane>
            </Tabs>
          </div>
        </div>
      </div>

      <Footer />
    </div>
  );
};

export default DetailPageTemplate;
