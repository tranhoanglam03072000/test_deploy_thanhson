import { zodResolver } from "@hookform/resolvers/zod";
import { PATH } from "constant/configPath";
import { SubmitHandler, useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { loginSchema, loginSchemaTypes } from "schema/LoginSchema";
// import { quanLyNguoiDungServices } from "services/quanLyNguoiDungServices";
import { useAppDispatch } from "store";
import { loginThunk } from "store/quanLyNguoiDung/thunk";

const LoginTemplate = () => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch()
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<loginSchemaTypes>({
    mode: "onChange",
    resolver: zodResolver(loginSchema)
  });

  const onSubmit: SubmitHandler<loginSchemaTypes> =async (value) => {
    // try {
    //   await quanLyNguoiDungServices.login(value)
    //   toast.success('Đăng nhập thành công')
    //   navigate('/')
    // } catch (err: any) {
    //   toast.error(err?.response?.data?.content)
    // }
    dispatch(loginThunk(value))
    .unwrap()
    .then(()=> {
      toast.success('Đăng nhập thành công')
      navigate('/')
    })
    .catch((err)=> {
      toast.error(err?.response?.data?.content)
    })

    

  }
  return (
    <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto md:h-screen lg:py-0">
      <div className="w-full bg-white rounded-lg shadow dark:border md:mt-0 sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700">
        <div className="p-6 space-y-4 md:space-y-6 sm:p-8">
          <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
            Đăng nhập
          </h1>
          <form className="space-y-4 md:space-y-6 " onSubmit={handleSubmit(onSubmit)} >
            <div>
              <label className="block mb-2 text-sm font-medium text-gray-900">
                Tài khoản
              </label>
              <input
                type="text"
                className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg block w-full p-2.5"
                {...register("taiKhoan")}
              />
              <p className="text-red-500">{errors?.taiKhoan?.message}</p>
            </div>
            <div>
              <label className="block mb-2 text-sm font-medium text-gray-900">
                Mật khẩu
              </label>
              <input
                type="password"
                className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg block w-full p-2.5"
                {...register("matKhau")}
              />
               <p className="text-red-500">{errors?.matKhau?.message}</p>
            </div>
            <div className="flex items-center justify-between">
              <div className="flex items-start">
                <div className="flex items-center h-5">
                  <input
                    id="remember"
                    aria-describedby="remember"
                    type="checkbox"
                    className="w-4 h-4 border border-gray-300 rounded bg-gray-50 outline-none"
                  />
                </div>
                <div className="ml-3 text-sm">
                  <label
                    htmlFor="remember"
                    className="text-gray-500 dark:text-gray-300"
                  >
                    Lưu mật khẩu
                  </label>
                </div>
              </div>
              <a
                href="#"
                className="text-sm font-medium text-primary-600 hover:underline dark:text-primary-500"
              >
                Quên mật khẩu?
              </a>
            </div>
            <div className="text-center">
              <button className=" text-white bg-blue-500 hover:bg-blue-700 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 w-full">
                Đăng nhập
              </button>
            </div>

            <p className="text-sm font-light text-gray-500 dark:text-gray-400">
              Bạn chưa có tài khoản?{" "}
              <span
                className="text-blue-500 font-bold cursor-pointer"
                onClick={() => {
                  navigate(PATH.register);
                }}
              >
                Đăng ký
              </span>
            </p>
          </form>
        </div>
      </div>
    </div>
  );
};

export default LoginTemplate;
