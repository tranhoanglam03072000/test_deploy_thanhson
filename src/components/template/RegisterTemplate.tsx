import { zodResolver } from "@hookform/resolvers/zod";
import { PATH } from "constant/configPath";
import { SubmitHandler, useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { RegisteSchema, RegisteSchemaTypes } from "schema";
import { quanLyNguoiDungServices } from "services/quanLyNguoiDungServices";

const RegisterTemplate = () => {
  const navigate = useNavigate();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<RegisteSchemaTypes>({
    mode: "onChange",
    resolver: zodResolver(RegisteSchema),
  });

  const onSubmit: SubmitHandler<RegisteSchemaTypes> = async (value) => {
    try {
      await quanLyNguoiDungServices.register(value);
      toast.success("Đăng ký thành công");
      navigate(PATH.login)
    } catch (err: any) {
      // console.log("error: ", error);

      toast.error(err?.response?.data?.content)
    }
  };
  return (
    <div>
      <div className="w-full bg-white rounded-lg shadow dark:border md:mt-0 sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700">
        <div className="p-6 space-y-4 md:space-y-6 sm:p-8">
          <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
            Đăng ký
          </h1>
          <form
            className="space-y-4 md:space-y-6"
            onSubmit={handleSubmit(onSubmit)}
          >
            <div>
              <input
                type="text"
                className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg block w-full p-2.5"
                placeholder="Tài khoản"
                {...register("taiKhoan")}
              />
              <p className="text-red-500">{errors?.taiKhoan?.message}</p>
            </div>
            <div>
              <input
                type="password"
                className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg block w-full p-2.5"
                placeholder="Mật khẩu"
                {...register("matKhau")}
              />
              <p className="text-red-500">{errors?.matKhau?.message}</p>
            </div>
            <div>
              <input
                type="text"
                className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg block w-full p-2.5"
                placeholder="Email"
                {...register("email")}
              />
              <p className="text-red-500">{errors?.email?.message}</p>
            </div>
            <div>
              <input
                type="text"
                className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg block w-full p-2.5"
                placeholder="Số điện thoại"
                {...register("soDt")}
              />
              <p className="text-red-500">{errors?.soDt?.message}</p>
            </div>
            <div>
              <input
                type="text"
                className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg block w-full p-2.5"
                placeholder="Mã nhóm"
                {...register("maNhom")}
              />
              <p className="text-red-500">{errors?.maNhom?.message}</p>
            </div>
            <div>
              <input
                type="text"
                className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg block w-full p-2.5"
                placeholder="Họ và tên"
                {...register("hoTen")}
              />
              <p className="text-red-500">{errors?.hoTen?.message}</p>
            </div>
            <div className="text-center">
              <button className=" text-white bg-blue-500 hover:bg-blue-700 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 w-full">
                Đăng ký
              </button>
            </div>

            <p className="text-sm font-light text-gray-500 dark:text-gray-400">
              Bạn đã có tài khoản?{" "}
              <span
                className="text-blue-500 font-bold cursor-pointer"
                onClick={() => {
                  navigate(PATH.login);
                }}
              >
                Đăng nhập
              </span>
            </p>
          </form>
        </div>
      </div>
    </div>
  );
};

export default RegisterTemplate;
