export const NewsTemplate = () => {
  return (
    <div className="mx-auto max-w-screen-xl">
      <p className="text-blue-500 text-center text-4xl font-bold mt-32">TIN TỨC</p>
      <section className="text-gray-600 body-font">
        <div className="container px-5 py-24 mx-auto">
          <div className="flex flex-wrap -m-4">
            <div className="p-4 md:w-1/3">
              <div className="h-full border-2 border-gray-200 border-opacity-60 rounded-lg overflow-hidden">
                <img
                  className="lg:h-48 md:h-36 w-full object-cover object-center"
                  src="../../../public/image/carousel/carousel1.jpg"
                  alt="blog"
                />
                <div className="p-6">

                  <h1 className="title-font text-lg font-medium text-gray-900 mb-3">
                  [Ác Quỷ Ma Sơ II] - Loạt Easter Egg rùng rợn nhất của THE NUN 2: Một cảnh cũ của Valak “tiên tri” tương lai của nhân vật bất ngờ?
                  </h1>
                  <p className="leading-relaxed mb-3">
                  Ngay từ cảnh đầu của The Nun 2, khán giả có thể tài tình phát hiện ra phim đã tinh tế tri ân đến "huyền thoại trừ tà" The Exorcist. Khi Valak xuất hiện và tước đi mạng sống của nhân vật cha xứ Noiret, hình bóng của quỷ dữ đã tràn ra ngoài phố đêm và "ký sinh" vào một gã đàn ông bí ẩn, đội nón và xách theo túi đồ nghề của mình. Nhân vật này chính là anh chàng Frenchie - người vốn dĩ đã bị Valak chiếm hữu từ đoạn cuối của phần 1...
                  </p>
                </div>
              </div>
            </div>
            <div className="p-4 md:w-1/3">
              <div className="h-full border-2 border-gray-200 border-opacity-60 rounded-lg overflow-hidden">
                <img
                  className="lg:h-48 md:h-36 w-full object-cover object-center"
                  src="../../../public/image/carousel/carousel1.jpg"
                  alt="blog"
                />
                <div className="p-6">
                  <h1 className="title-font text-lg font-medium text-gray-900 mb-3">
                  [EXPEND4BLE - BIỆT ĐỘI ĐÁNH THUÊ 4] - Rapper Binz cùng bản hit BigCityBoi sẽ xuất hiện trong bom tấn hành động công phá phòng vé tháng 9 “Biệt Đội Đánh Thuê 4”
                  </h1>
                  <p className="leading-relaxed mb-3">
                  Ngày 12/09, phía nhà phát hành CJ CGV Việt Nam và SpaceSpeakers Group xác nhận bản hit "BigCityBoi" do rapper Binz thể hiện sẽ được sử dụng chính thức trong bom tấn hành động Biệt Đội Đánh Thuê 4. Đây là lần đầu tiên một ca khúc tiếng Việt được sử dụng trong loạt phim đình đám của Hollywood. 
                  </p>
                </div>
              </div>
            </div>
            <div className="p-4 md:w-1/3">
              <div className="h-full border-2 border-gray-200 border-opacity-60 rounded-lg overflow-hidden">
                <img
                  className="lg:h-48 md:h-36 w-full object-cover object-center"
                  src="../../../public/image/carousel/carousel1.jpg"
                  alt="blog"
                />
                <div className="p-6">

                  <h1 className="title-font text-lg font-medium text-gray-900 mb-3">
                  [Shin - Cậu Bé Bút Chì: Đại Chiến Siêu Năng Lực Sushi Bay] - Thương hiệu “Shin - Cậu Bé Bút Chì” công phá màn ảnh rộng với phần phim 3D đầu tiên, “chốt đơn” khởi chiếu 25.08
                  </h1>
                  <p className="leading-relaxed mb-3">
                  3DCG! Shin - Cậu Bé Bút Chì: Đại Chiến Siêu Năng Lực ~Sushi Bay~ xoay quanh câu chuyện về hai nguồn sáng đặc biệt từ vũ trụ mang theo siêu năng lực đặc biệt tới Trái Đất. Một nguồn sáng tích cực "nhập" vào nhóc Shin, khiến cặp mông núng nính của cậu chàng trở nên nóng bỏng và có khả năng điều khiển những đồ vật xung quanh theo ý muốn. 
                  </p>
                  <div className="flex items-center flex-wrap ">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default NewsTemplate;
