import React, { Fragment, useEffect } from "react";
import { Button, Table } from "antd";
import type { ColumnsType, TableProps } from "antd/es/table";
import { Input,  } from "antd";
import { useSelector } from "react-redux";
import { RootState, useAppDispatch } from "store";
import { getMovieListThunk } from "store/quanLyPhim/thunk";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { NavLink, generatePath, useNavigate } from "react-router-dom";
import { PATH } from "constant";


const { Search } = Input;

interface DataType {
  key: React.Key;
  maPhim: number;
  tenPhim: string;
  hinhAnh: string;
  moTa: string
}

const onSearch = (value: any) => console.log(value);

export const FilmTemplate = () => {
  const navigate = useNavigate()
  

  const columns: ColumnsType<DataType> = [
    {
      title: "Mã phim",
      dataIndex: "maPhim",
      width: '10%',
      sorter: (a, b) => a.maPhim - b.maPhim,
    },
    {
      title: "Tên phim",
      dataIndex: "tenPhim",
      width: '20%',
     
    },
    {
      title: "Hình ảnh",
      dataIndex: "hinhAnh",
      render: (text :any, film: any) => {return <Fragment>
        <img src={film.hinhAnh} width={100} ></img>
      </Fragment>},
      width: "20%",
    },
    {
      title: "Mô tả",
      dataIndex: "moTa",
      render: (text, film: any ) => { return <Fragment>{film.moTa.length > 200 ? film.moTa.substring(0, 200) + '...' : film.moTa}</Fragment>
        
      },
      width: "40%",
    },
    {
      title: "Hành động",
      dataIndex: "hanhDong",
      render: (text, film) => {
        const path  = generatePath(PATH.editfilm, {id:film.maPhim})
        return <Fragment>
            <div className="text-center">
            <NavLink to={path} className=" text-white"><EditOutlined/></NavLink>
            <NavLink to='/' className=" !text-red-500 p-1 ml-2"><DeleteOutlined/></NavLink>

            </div>
        </Fragment>
      },
      width: "10%",
    },
  ];

  const { movieList } = useSelector(
    (state: RootState) => state.quanLyPhimReducer
  );
  console.log("movieList: ", movieList);

  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getMovieListThunk());
  }, [dispatch]);

  const data: DataType[] = movieList

  const onChange: TableProps<DataType>["onChange"] = (
    pagination,
    filters,
    sorter,
    extra
  ) => {
    console.log("params", pagination, filters, sorter, extra);
  };

  return (
    <div>
      <h1 className="text-4xl font-bold py-4">Quản lý phim</h1>
      <button onClick={()=> {
        navigate(PATH.addfilm)
      }} className="my-2 bg-blue-500 p-2 text-white rounded-md">Thêm phim</button>
      <Search
        className="mb-6"
        placeholder="Nhập nội dung tìm kiếm"
        enterButton="Tìm kiếm"
        size="large"
        onSearch={onSearch}
      />
      <Table columns={columns} dataSource={data} onChange={onChange} />
    </div>
  );
};

export default FilmTemplate;
