import { apiInstance } from 'constant'
import { Banner, Movie, addMovie, editMovie } from 'types'

const api = apiInstance({
    baseURL: import.meta.env.VITE_QUAN_LY_PHIM_API
})
export const quanLyPhimServices = {
    getBanner: () => api.get<ApiResponse<Banner>>('/LayDanhSachBanner'),

    getMovieList: () => api.get<ApiResponse<Movie>>('/LayDanhSachPhim'),

    addMovie: (payload: addMovie) => api.post<ApiResponse<addMovie>>('/ThemPhimUploadHinh', payload),
  
    editMovie: (maPhim: any) => api.get<ApiResponse<editMovie>>(`/LayThongTinPhim?MaPhim=${maPhim}`)
}