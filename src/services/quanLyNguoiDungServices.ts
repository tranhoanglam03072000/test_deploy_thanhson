import { apiInstance } from "constant/apiInstance";
import { AccountShemaTypes, RegisteSchemaTypes } from "schema";
import { loginSchemaTypes } from "schema/LoginSchema";
import { User, UserInfo } from "types";



const api = apiInstance({
    baseURL: import.meta.env.VITE_QUAN_LY_NGUOI_DUNG_API
})


export const quanLyNguoiDungServices = {
    register: (payload: RegisteSchemaTypes) => api.post('/DangKy',payload),

    login: (payload: loginSchemaTypes) => api.post<ApiResponse<User>>('/DangNhap', payload),

    getUser: () => api.post<ApiResponse<UserInfo>>('/ThongTinTaiKhoan'),

    updateUser: (payload: AccountShemaTypes) => api.put('/CapNhatThongTinNguoiDung', payload)
}